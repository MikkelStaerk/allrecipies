var app;
(function (app) {
    var services;
    (function (services) {
        function IngrediensService(DB, $firebaseArray) {
            return $firebaseArray(DB.child('ingrediens'));
        }
        services.IngrediensService = IngrediensService;
        IngrediensService.$inject = ['firebaseFactory', '$firebaseArray'];
        angular.module('app.services').factory('IngrediensService', IngrediensService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
