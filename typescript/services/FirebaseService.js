var app;
(function (app) {
    var services;
    (function (services) {
        function FirebaseService() {
            return new Firebase('https://my-recipies.firebaseio.com');
        }
        services.FirebaseService = FirebaseService;
        angular.module('app.services').factory('firebaseFactory', FirebaseService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
