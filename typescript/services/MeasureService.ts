module app.services
{
    export function MeasureService (DB: Firebase, $firebaseArray: AngularFireArrayService)
    {
        return $firebaseArray(DB.child('measures'));
    }

    MeasureService.$inject = ['firebaseFactory', '$firebaseArray'];

    angular.module('app.services').factory('MeasureService', MeasureService);
}
