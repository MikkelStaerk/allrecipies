module app.services
{
    export function RecipeService (DB: Firebase, $firebaseArray: AngularFireArrayService)
    {
        return $firebaseArray(DB.child('recipies'));
    }

    RecipeService.$inject = ['firebaseFactory', '$firebaseArray'];

    angular.module('app.services').factory('RecipeService', RecipeService);
}
