module app.services
{
    export function IngrediensService (DB: Firebase, $firebaseArray: AngularFireArrayService)
    {
        return $firebaseArray(DB.child('ingrediens'));
    }

    IngrediensService.$inject = ['firebaseFactory', '$firebaseArray'];

    angular.module('app.services').factory('IngrediensService', IngrediensService);
}
