var app;
(function (app) {
    var services;
    (function (services) {
        function RecipeService(DB, $firebaseArray) {
            return $firebaseArray(DB.child('recipies'));
        }
        services.RecipeService = RecipeService;
        RecipeService.$inject = ['firebaseFactory', '$firebaseArray'];
        angular.module('app.services').factory('RecipeService', RecipeService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
