/// <reference path="../_all.ts"/>

module app.services
{
    export function FirebaseService ()
    {
        return new Firebase('https://my-recipies.firebaseio.com');
    }
    angular.module('app.services').factory('firebaseFactory', FirebaseService);
}
