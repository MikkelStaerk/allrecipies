var app;
(function (app) {
    var services;
    (function (services) {
        function MeasureService(DB, $firebaseArray) {
            return $firebaseArray(DB.child('measures'));
        }
        services.MeasureService = MeasureService;
        MeasureService.$inject = ['firebaseFactory', '$firebaseArray'];
        angular.module('app.services').factory('MeasureService', MeasureService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
