module app.filters {
    export function FormatTimeFilter () {

        function filter (time:number) {
            return time.toString() + " minutter";
        }

        filter['$stateful'] = true;

        return filter;
    }


    angular.module('app.filters').filter('FormatTimeFilter', FormatTimeFilter);
}
