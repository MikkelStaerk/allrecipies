var app;
(function (app) {
    var filters;
    (function (filters) {
        function FormatTimeFilter() {
            function filter(time) {
                return time.toString() + " minutter";
            }
            filter['$stateful'] = true;
            return filter;
        }
        filters.FormatTimeFilter = FormatTimeFilter;
        angular.module('app.filters').filter('FormatTimeFilter', FormatTimeFilter);
    })(filters = app.filters || (app.filters = {}));
})(app || (app = {}));
