/// <reference path="../_all.ts"/>

module app.controllers {

    import IngrediensViewModel = app.IngrediensViewModel;
    import MeasureViewModel = app.MeasureViewModel;
    import IngrediensService = app.services.IngrediensService;
    import MeasureService = app.services.MeasureService;
    import RecipeService = app.services.RecipeService;
    import RecipeViewModel = app.RecipeViewModel;

    export class RecipeCtrl {

        private recipeId:string;
        private Recipe:RecipeViewModel;

        private getIngrediens = (id:string) : void => {
            var ingrediens = this.ingredienses.$getRecord(id);
            return ingrediens;
        }
        private getMeasure = (id:string) : void => {
            var ingrediens = this.ingredienses.$getRecord(id);
            return this.measures.$getRecord(ingrediens.MeasureId);
        }

        constructor(private recipies:any, private ingredienses:any,  private measures:any, private routeParameters) {
                this.recipeId = routeParameters.recipeId;
                this.Recipe = this.recipies.$getRecord(this.recipeId);
        }


        public static $inject = ["RecipeService","IngrediensService", "MeasureService", "$routeParams"];

    }

    angular.module('app.controllers').controller('RecipeCtrl', RecipeCtrl);
}
