var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeCtrl = (function () {
            function RecipeCtrl(recipies, ingredienses, measures, routeParameters) {
                var _this = this;
                this.recipies = recipies;
                this.ingredienses = ingredienses;
                this.measures = measures;
                this.routeParameters = routeParameters;
                this.getIngrediens = function (id) {
                    var ingrediens = _this.ingredienses.$getRecord(id);
                    return ingrediens;
                };
                this.getMeasure = function (id) {
                    var ingrediens = _this.ingredienses.$getRecord(id);
                    return _this.measures.$getRecord(ingrediens.MeasureId);
                };
                this.recipeId = routeParameters.recipeId;
                this.Recipe = this.recipies.$getRecord(this.recipeId);
            }
            RecipeCtrl.$inject = ["RecipeService", "IngrediensService", "MeasureService", "$routeParams"];
            return RecipeCtrl;
        })();
        controllers.RecipeCtrl = RecipeCtrl;
        angular.module('app.controllers').controller('RecipeCtrl', RecipeCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
