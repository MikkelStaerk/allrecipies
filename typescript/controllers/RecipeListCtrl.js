var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeListCtrl = (function () {
            function RecipeListCtrl(recipies, ingredienses, measures) {
                this.recipies = recipies;
                this.ingredienses = ingredienses;
                this.measures = measures;
            }
            RecipeListCtrl.$inject = ["RecipeService", "IngrediensService", "MeasureService"];
            return RecipeListCtrl;
        })();
        controllers.RecipeListCtrl = RecipeListCtrl;
        angular.module('app.controllers').controller('RecipeListCtrl', RecipeListCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
