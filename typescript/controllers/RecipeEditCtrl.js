var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeViewModel = app.RecipeViewModel;
        var RecipeEditCtrl = (function () {
            function RecipeEditCtrl(recipies, ingredienses, measures, routeParameters, firebaseObject, location) {
                var _this = this;
                this.recipies = recipies;
                this.ingredienses = ingredienses;
                this.measures = measures;
                this.routeParameters = routeParameters;
                this.location = location;
                this.gotoList = function () {
                    _this.location.path("recipelist");
                };
                this.getMeasure = function (id) {
                    return _this.measures.$getRecord(id);
                };
                this.addIngrediens = function () {
                    if (_this.Recipe.Ingrediens == undefined) {
                        _this.Recipe.Ingrediens = new Array();
                    }
                    _this.Recipe.Ingrediens.push(_this.newIngrediens);
                    _this.newIngrediens = new app.RecipeIngrediensViewModel();
                };
                this.save = function () {
                    if (_this.recipeId === "new") {
                        _this.recipies.$add(_this.Recipe).then(_this.gotoList);
                    }
                    else {
                        _this.recipies.$save(_this.Recipe).then(_this.gotoList);
                    }
                };
                this.newIngrediens = new app.RecipeIngrediensViewModel();
                this.recipeId = routeParameters.recipeId;
                if (this.recipeId === "new") {
                    this.Recipe = new RecipeViewModel();
                }
                else {
                    this.Recipe = this.recipies.$getRecord(this.recipeId);
                }
            }
            RecipeEditCtrl.$inject = ["RecipeService", "IngrediensService", "MeasureService", "$routeParams", "$firebaseObject", "$location"];
            return RecipeEditCtrl;
        })();
        controllers.RecipeEditCtrl = RecipeEditCtrl;
        angular.module('app.controllers').controller('RecipeEditCtrl', RecipeEditCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
