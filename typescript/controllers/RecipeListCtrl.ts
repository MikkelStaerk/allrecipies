/// <reference path="../_all.ts"/>

module app.controllers {

    import IngrediensViewModel = app.IngrediensViewModel;
    import MeasureViewModel = app.MeasureViewModel;
    import IngrediensService = app.services.IngrediensService;
    import MeasureService = app.services.MeasureService;
    import RecipeService = app.services.RecipeService;
    import RecipeViewModel = app.RecipeViewModel;

    export class RecipeListCtrl {

        constructor(private recipies:any, private ingredienses:any,  private measures:any) {
        }


        public static $inject = ["RecipeService","IngrediensService", "MeasureService"];

    }

    angular.module('app.controllers').controller('RecipeListCtrl', RecipeListCtrl);
}
