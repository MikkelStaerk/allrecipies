/// <reference path="../_all.ts"/>

module app.controllers {

    import IngrediensViewModel = app.IngrediensViewModel;
    import MeasureViewModel = app.MeasureViewModel;
    import IngrediensService = app.services.IngrediensService;
    import MeasureService = app.services.MeasureService;
    import RecipeService = app.services.RecipeService;
    import RecipeViewModel = app.RecipeViewModel;

    export class RecipeEditCtrl {

        private recipeId:string;
        private Recipe:any;
        private newIngrediens:RecipeIngrediensViewModel;

        private gotoList = () : void => {
            this.location.path("recipelist");
        }

        private getMeasure = (id:string) : void => {
            // var ingrediens = this.ingredienses.$getRecord(id);
            return this.measures.$getRecord(id);
        }

        private addIngrediens = () : void => {
            if(this.Recipe.Ingrediens==undefined) {
                this.Recipe.Ingrediens = new Array<RecipeIngrediensViewModel>();
            }

            this.Recipe.Ingrediens.push(this.newIngrediens);

            this.newIngrediens = new RecipeIngrediensViewModel();

        }

        private save = () : void => {
            if(this.recipeId==="new") {
                this.recipies.$add(this.Recipe).then(this.gotoList);
            } else {
                this.recipies.$save(this.Recipe).then(this.gotoList);
            }
        }

        constructor(private recipies:any, private ingredienses:any,  private measures:any, private routeParameters:any, firebaseObject:AngularFireObject, private location) {
                this.newIngrediens = new RecipeIngrediensViewModel();

                this.recipeId = routeParameters.recipeId;
                if(this.recipeId==="new") {
                    this.Recipe = new RecipeViewModel();
                } else {
                    this.Recipe = this.recipies.$getRecord(this.recipeId);
                }
        }


        public static $inject = ["RecipeService","IngrediensService", "MeasureService", "$routeParams", "$firebaseObject", "$location"];

    }

    angular.module('app.controllers').controller('RecipeEditCtrl', RecipeEditCtrl);
}
