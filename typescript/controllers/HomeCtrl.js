var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var HomeCtrl = (function () {
            function HomeCtrl() {
            }
            HomeCtrl.$inject = [];
            return HomeCtrl;
        })();
        controllers.HomeCtrl = HomeCtrl;
        angular.module('app.controllers').controller('HomeCtrl', HomeCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
