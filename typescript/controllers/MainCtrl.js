var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeViewModel = app.RecipeViewModel;
        var MainCtrl = (function () {
            function MainCtrl(recipies) {
                this.recipies = recipies;
                this.addRecipe = function () {
                    var recipe = new RecipeViewModel();
                    recipe.Title = "Burger";
                    recipe.TimeToComplete = 45;
                    recipe.WorkTime = 30;
                };
                this.addRecipe();
            }
            MainCtrl.$inject = ['RecipeService'];
            return MainCtrl;
        })();
        controllers.MainCtrl = MainCtrl;
        angular.module('app.controllers').controller('MainCtrl', MainCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
