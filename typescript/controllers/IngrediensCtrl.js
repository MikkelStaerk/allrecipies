var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var IngrediensViewModel = app.IngrediensViewModel;
        var IngrediensCtrl = (function () {
            function IngrediensCtrl(ingredienses, measures) {
                var _this = this;
                this.ingredienses = ingredienses;
                this.measures = measures;
                this.addIngrediens = function () {
                    _this.ingredienses.$add(_this.newIngrediens);
                    _this.newIngrediens = new IngrediensViewModel();
                };
                this.newIngrediens = new IngrediensViewModel();
            }
            IngrediensCtrl.prototype.getMeasureById = function (id) {
                var measure = this.measures.$getRecord(id);
                return measure;
            };
            IngrediensCtrl.$inject = ["IngrediensService", "MeasureService"];
            return IngrediensCtrl;
        })();
        controllers.IngrediensCtrl = IngrediensCtrl;
        angular.module('app.controllers').controller('IngrediensCtrl', IngrediensCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
