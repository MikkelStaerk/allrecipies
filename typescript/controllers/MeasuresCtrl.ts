/// <reference path="../_all.ts"/>

module app.controllers {

    import MeasureViewModel = app.MeasureViewModel;
    import MeasureService = app.services.MeasureService;

    export class MeasuresCtrl {

        private newMeasure:MeasureViewModel;

        public addMeasure = () : void =>
        {
            this.measures.$add(this.newMeasure);

            this.newMeasure = new MeasureViewModel();
        }

        constructor(private measures:any) {
            this.newMeasure = new MeasureViewModel();
        }

        public static $inject = ["MeasureService"];

    }

    angular.module('app.controllers').controller('MeasuresCtrl', MeasuresCtrl);
}
