/// <reference path="../_all.ts"/>

module app.controllers {

    import RecipeViewModel = app.RecipeViewModel;
    import RecipeService = app.services.RecipeService;

    export class MainCtrl {

        public addRecipe = () : void =>
        {

            var recipe = new RecipeViewModel();
            recipe.Title="Burger";
            recipe.TimeToComplete = 45;
            recipe.WorkTime = 30;

            // this.recipies.$add(recipe);
        }


        constructor(private recipies: any) {
            // $scope.title="Min titel";
            this.addRecipe();
        }

        public static $inject = ['RecipeService'];

    }

    angular.module('app.controllers').controller('MainCtrl', MainCtrl);
}
