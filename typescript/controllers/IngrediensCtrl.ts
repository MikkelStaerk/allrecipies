/// <reference path="../_all.ts"/>

module app.controllers {

    import IngrediensViewModel = app.IngrediensViewModel;
    import MeasureViewModel=app.MeasureViewModel;
    import IngrediensService = app.services.IngrediensService;
    import MeasureService = app.services.MeasureService;

    export class IngrediensCtrl {

        private newIngrediens:IngrediensViewModel;

        private getMeasureById(id:string) {
            var measure = this.measures.$getRecord(id);
            return measure;
        }

        public addIngrediens = () : void =>
        {
            this.ingredienses.$add(this.newIngrediens);

            this.newIngrediens = new IngrediensViewModel();
        }

        constructor(private ingredienses:any,  private measures:any) {
            this.newIngrediens = new IngrediensViewModel();
        }

        public static $inject = ["IngrediensService", "MeasureService"];

    }

    angular.module('app.controllers').controller('IngrediensCtrl', IngrediensCtrl);
}
