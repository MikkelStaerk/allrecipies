var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var MeasureViewModel = app.MeasureViewModel;
        var MeasuresCtrl = (function () {
            function MeasuresCtrl(measures) {
                var _this = this;
                this.measures = measures;
                this.addMeasure = function () {
                    _this.measures.$add(_this.newMeasure);
                    _this.newMeasure = new MeasureViewModel();
                };
                this.newMeasure = new MeasureViewModel();
            }
            MeasuresCtrl.$inject = ["MeasureService"];
            return MeasuresCtrl;
        })();
        controllers.MeasuresCtrl = MeasuresCtrl;
        angular.module('app.controllers').controller('MeasuresCtrl', MeasuresCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
