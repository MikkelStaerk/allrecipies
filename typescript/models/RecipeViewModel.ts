/// <reference path="../_all.ts" />

module app {

    export class RecipeViewModel {

        public Title:string;
        public TimeToComplete:number;
        public WorkTime:number;
        public Procedure:string;

        public Ingrediens:Array<RecipeIngrediensViewModel>;

        constructor() {

        }

    }

}
