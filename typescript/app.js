var modules = ['app.controllers', 'app.services', 'app.filters'];
modules.forEach(function (module) { return angular.module(module, []); });
modules.push('firebase');
modules.push('ngRoute');
var recipeApp = angular.module("recipeApp", modules)
    .config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/home', {
        templateUrl: '/typescript/templates/home.html',
        controller: 'HomeCtrl',
        controllerAs: "home"
    })
        .when('/measures', {
        templateUrl: '/typescript/templates/measures.html',
        controller: 'MeasuresCtrl',
        controllerAs: "measures"
    })
        .when('/ingrediens', {
        templateUrl: '/typescript/templates/ingrediens.html',
        controller: 'IngrediensCtrl',
        controllerAs: "ingrediens"
    })
        .when('/recipelist', {
        templateUrl: '/typescript/templates/recipelist.html',
        controller: 'RecipeListCtrl',
        controllerAs: "recipelist"
    })
        .when('/recipe/:recipeId', {
        templateUrl: '/typescript/templates/recipe.html',
        controller: 'RecipeCtrl',
        controllerAs: "recipe"
    })
        .when('/recipe/:recipeId/edit', {
        templateUrl: '/typescript/templates/recipeedit.html',
        controller: 'RecipeEditCtrl',
        controllerAs: "recipe"
    });
    $locationProvider.html5Mode(false);
});
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        null;
    })(controllers = app.controllers || (app.controllers = {}));
    var services;
    (function (services) {
        null;
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
