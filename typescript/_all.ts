// 3rd party
/// <reference path="../typings/tsd.d.ts" />

// Models
/// <reference path="./models/IngrediensViewModel.ts"/>
/// <reference path="./models/MeasureViewModel.ts"/>
/// <reference path="./models/RecipeIngrediensViewModel.ts"/>
/// <reference path="./models/RecipeViewModel.ts"/>

// services
/// <reference path="./services/FirebaseService.ts"/>

// Controllers
/// <reference path="./controllers/MainCtrl.ts"/>
