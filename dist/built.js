/// <reference path="../_all.ts" />
var app;
(function (app) {
    var IngrediensViewModel = (function () {
        function IngrediensViewModel() {
        }
        return IngrediensViewModel;
    })();
    app.IngrediensViewModel = IngrediensViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts" />
var app;
(function (app) {
    var MeasureViewModel = (function () {
        function MeasureViewModel() {
        }
        return MeasureViewModel;
    })();
    app.MeasureViewModel = MeasureViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts" />
var app;
(function (app) {
    var RecipeIngrediensViewModel = (function () {
        function RecipeIngrediensViewModel() {
        }
        return RecipeIngrediensViewModel;
    })();
    app.RecipeIngrediensViewModel = RecipeIngrediensViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts" />
var app;
(function (app) {
    var RecipeViewModel = (function () {
        function RecipeViewModel() {
        }
        return RecipeViewModel;
    })();
    app.RecipeViewModel = RecipeViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var services;
    (function (services) {
        function FirebaseService() {
            return new Firebase('https://my-recipies.firebaseio.com');
        }
        services.FirebaseService = FirebaseService;
        angular.module('app.services').factory('firebaseFactory', FirebaseService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeViewModel = app.RecipeViewModel;
        var MainCtrl = (function () {
            function MainCtrl(recipies) {
                this.recipies = recipies;
                this.addRecipe = function () {
                    var recipe = new RecipeViewModel();
                    recipe.Title = "Burger";
                    recipe.TimeToComplete = 45;
                    recipe.WorkTime = 30;
                };
                this.addRecipe();
            }
            MainCtrl.$inject = ['RecipeService'];
            return MainCtrl;
        })();
        controllers.MainCtrl = MainCtrl;
        angular.module('app.controllers').controller('MainCtrl', MainCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
// 3rd party
/// <reference path="../typings/tsd.d.ts" />
/// <reference path="./models/IngrediensViewModel.ts"/>
/// <reference path="./models/MeasureViewModel.ts"/>
/// <reference path="./models/RecipeIngrediensViewModel.ts"/>
/// <reference path="./models/RecipeViewModel.ts"/>
/// <reference path="./services/FirebaseService.ts"/>
/// <reference path="./controllers/MainCtrl.ts"/>
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var HomeCtrl = (function () {
            function HomeCtrl() {
            }
            HomeCtrl.$inject = [];
            return HomeCtrl;
        })();
        controllers.HomeCtrl = HomeCtrl;
        angular.module('app.controllers').controller('HomeCtrl', HomeCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var IngrediensViewModel = app.IngrediensViewModel;
        var IngrediensCtrl = (function () {
            function IngrediensCtrl(ingredienses, measures) {
                var _this = this;
                this.ingredienses = ingredienses;
                this.measures = measures;
                this.addIngrediens = function () {
                    _this.ingredienses.$add(_this.newIngrediens);
                    _this.newIngrediens = new IngrediensViewModel();
                };
                this.newIngrediens = new IngrediensViewModel();
            }
            IngrediensCtrl.prototype.getMeasureById = function (id) {
                var measure = this.measures.$getRecord(id);
                return measure;
            };
            IngrediensCtrl.$inject = ["IngrediensService", "MeasureService"];
            return IngrediensCtrl;
        })();
        controllers.IngrediensCtrl = IngrediensCtrl;
        angular.module('app.controllers').controller('IngrediensCtrl', IngrediensCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var MeasureViewModel = app.MeasureViewModel;
        var MeasuresCtrl = (function () {
            function MeasuresCtrl(measures) {
                var _this = this;
                this.measures = measures;
                this.addMeasure = function () {
                    _this.measures.$add(_this.newMeasure);
                    _this.newMeasure = new MeasureViewModel();
                };
                this.newMeasure = new MeasureViewModel();
            }
            MeasuresCtrl.$inject = ["MeasureService"];
            return MeasuresCtrl;
        })();
        controllers.MeasuresCtrl = MeasuresCtrl;
        angular.module('app.controllers').controller('MeasuresCtrl', MeasuresCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeCtrl = (function () {
            function RecipeCtrl(recipies, ingredienses, measures, routeParameters) {
                var _this = this;
                this.recipies = recipies;
                this.ingredienses = ingredienses;
                this.measures = measures;
                this.routeParameters = routeParameters;
                this.getIngrediens = function (id) {
                    var ingrediens = _this.ingredienses.$getRecord(id);
                    return ingrediens;
                };
                this.getMeasure = function (id) {
                    var ingrediens = _this.ingredienses.$getRecord(id);
                    return _this.measures.$getRecord(ingrediens.MeasureId);
                };
                this.recipeId = routeParameters.recipeId;
                this.Recipe = this.recipies.$getRecord(this.recipeId);
            }
            RecipeCtrl.$inject = ["RecipeService", "IngrediensService", "MeasureService", "$routeParams"];
            return RecipeCtrl;
        })();
        controllers.RecipeCtrl = RecipeCtrl;
        angular.module('app.controllers').controller('RecipeCtrl', RecipeCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeViewModel = app.RecipeViewModel;
        var RecipeEditCtrl = (function () {
            function RecipeEditCtrl(recipies, ingredienses, measures, routeParameters, firebaseObject, location) {
                var _this = this;
                this.recipies = recipies;
                this.ingredienses = ingredienses;
                this.measures = measures;
                this.routeParameters = routeParameters;
                this.location = location;
                this.gotoList = function () {
                    _this.location.path("recipelist");
                };
                this.getMeasure = function (id) {
                    return _this.measures.$getRecord(id);
                };
                this.addIngrediens = function () {
                    if (_this.Recipe.Ingrediens == undefined) {
                        _this.Recipe.Ingrediens = new Array();
                    }
                    _this.Recipe.Ingrediens.push(_this.newIngrediens);
                    _this.newIngrediens = new app.RecipeIngrediensViewModel();
                };
                this.save = function () {
                    if (_this.recipeId === "new") {
                        _this.recipies.$add(_this.Recipe).then(_this.gotoList);
                    }
                    else {
                        _this.recipies.$save(_this.Recipe).then(_this.gotoList);
                    }
                };
                this.newIngrediens = new app.RecipeIngrediensViewModel();
                this.recipeId = routeParameters.recipeId;
                if (this.recipeId === "new") {
                    this.Recipe = new RecipeViewModel();
                }
                else {
                    this.Recipe = this.recipies.$getRecord(this.recipeId);
                }
            }
            RecipeEditCtrl.$inject = ["RecipeService", "IngrediensService", "MeasureService", "$routeParams", "$firebaseObject", "$location"];
            return RecipeEditCtrl;
        })();
        controllers.RecipeEditCtrl = RecipeEditCtrl;
        angular.module('app.controllers').controller('RecipeEditCtrl', RecipeEditCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeListCtrl = (function () {
            function RecipeListCtrl(recipies, ingredienses, measures) {
                this.recipies = recipies;
                this.ingredienses = ingredienses;
                this.measures = measures;
            }
            RecipeListCtrl.$inject = ["RecipeService", "IngrediensService", "MeasureService"];
            return RecipeListCtrl;
        })();
        controllers.RecipeListCtrl = RecipeListCtrl;
        angular.module('app.controllers').controller('RecipeListCtrl', RecipeListCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
var app;
(function (app) {
    var filters;
    (function (filters) {
        function FormatTimeFilter() {
            function filter(time) {
                return time.toString() + " minutter";
            }
            filter['$stateful'] = true;
            return filter;
        }
        filters.FormatTimeFilter = FormatTimeFilter;
        angular.module('app.filters').filter('FormatTimeFilter', FormatTimeFilter);
    })(filters = app.filters || (app.filters = {}));
})(app || (app = {}));
var app;
(function (app) {
    var services;
    (function (services) {
        function IngrediensService(DB, $firebaseArray) {
            return $firebaseArray(DB.child('ingrediens'));
        }
        services.IngrediensService = IngrediensService;
        IngrediensService.$inject = ['firebaseFactory', '$firebaseArray'];
        angular.module('app.services').factory('IngrediensService', IngrediensService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
var app;
(function (app) {
    var services;
    (function (services) {
        function MeasureService(DB, $firebaseArray) {
            return $firebaseArray(DB.child('measures'));
        }
        services.MeasureService = MeasureService;
        MeasureService.$inject = ['firebaseFactory', '$firebaseArray'];
        angular.module('app.services').factory('MeasureService', MeasureService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
var app;
(function (app) {
    var services;
    (function (services) {
        function RecipeService(DB, $firebaseArray) {
            return $firebaseArray(DB.child('recipies'));
        }
        services.RecipeService = RecipeService;
        RecipeService.$inject = ['firebaseFactory', '$firebaseArray'];
        angular.module('app.services').factory('RecipeService', RecipeService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
//# sourceMappingURL=built.js.map