/// <reference path="../_all.ts" />
var app;
(function (app) {
    var IngrediensViewModel = (function () {
        function IngrediensViewModel() {
        }
        return IngrediensViewModel;
    })();
    app.IngrediensViewModel = IngrediensViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts" />
var app;
(function (app) {
    var MeasureViewModel = (function () {
        function MeasureViewModel() {
        }
        return MeasureViewModel;
    })();
    app.MeasureViewModel = MeasureViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts" />
var app;
(function (app) {
    var RecipeIngrediensViewModel = (function () {
        function RecipeIngrediensViewModel() {
        }
        return RecipeIngrediensViewModel;
    })();
    app.RecipeIngrediensViewModel = RecipeIngrediensViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts" />
var app;
(function (app) {
    var RecipeViewModel = (function () {
        function RecipeViewModel() {
        }
        return RecipeViewModel;
    })();
    app.RecipeViewModel = RecipeViewModel;
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var services;
    (function (services) {
        function FirebaseService() {
            return new Firebase('https://my-recipies.firebaseio.com');
        }
        services.FirebaseService = FirebaseService;
        angular.module('app.services').factory('firebaseFactory', FirebaseService);
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
/// <reference path="../_all.ts"/>
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        var RecipeViewModel = app.RecipeViewModel;
        var MainCtrl = (function () {
            function MainCtrl(recipies) {
                this.recipies = recipies;
                this.addRecipe = function () {
                    var recipe = new RecipeViewModel();
                    recipe.Title = "Burger";
                    recipe.TimeToComplete = 45;
                    recipe.WorkTime = 30;
                };
                this.addRecipe();
            }
            MainCtrl.$inject = ['RecipeService'];
            return MainCtrl;
        })();
        controllers.MainCtrl = MainCtrl;
        angular.module('app.controllers').controller('MainCtrl', MainCtrl);
    })(controllers = app.controllers || (app.controllers = {}));
})(app || (app = {}));
// 3rd party
/// <reference path="../typings/tsd.d.ts" />
/// <reference path="./models/IngrediensViewModel.ts"/>
/// <reference path="./models/MeasureViewModel.ts"/>
/// <reference path="./models/RecipeIngrediensViewModel.ts"/>
/// <reference path="./models/RecipeViewModel.ts"/>
/// <reference path="./services/FirebaseService.ts"/>
/// <reference path="./controllers/MainCtrl.ts"/>
/// <reference path="_all.ts" />
var modules = ['app.controllers', 'app.services', 'app.filters'];
modules.forEach(function (module) { return angular.module(module, []); });
modules.push('firebase');
modules.push('ngRoute');
var recipeApp = angular.module("recipeApp", modules)
    .config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/home', {
        templateUrl: '/typescript/templates/home.html',
        controller: 'HomeCtrl',
        controllerAs: "home"
    })
        .when('/measures', {
        templateUrl: '/typescript/templates/measures.html',
        controller: 'MeasuresCtrl',
        controllerAs: "measures"
    })
        .when('/ingrediens', {
        templateUrl: '/typescript/templates/ingrediens.html',
        controller: 'IngrediensCtrl',
        controllerAs: "ingrediens"
    })
        .when('/recipelist', {
        templateUrl: '/typescript/templates/recipelist.html',
        controller: 'RecipeListCtrl',
        controllerAs: "recipelist"
    })
        .when('/recipe/:recipeId', {
        templateUrl: '/typescript/templates/recipe.html',
        controller: 'RecipeCtrl',
        controllerAs: "recipe"
    })
        .when('/recipe/:recipeId/edit', {
        templateUrl: '/typescript/templates/recipeedit.html',
        controller: 'RecipeEditCtrl',
        controllerAs: "recipe"
    });
    $locationProvider.html5Mode(false);
});
var app;
(function (app) {
    var controllers;
    (function (controllers) {
        null;
    })(controllers = app.controllers || (app.controllers = {}));
    var services;
    (function (services) {
        null;
    })(services = app.services || (app.services = {}));
})(app || (app = {}));
//# sourceMappingURL=app.js.map