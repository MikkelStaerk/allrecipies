module.exports = function(grunt) {


  grunt.initConfig({
      sass: {                              // Task
          dist: {                            // Target
            options: {                       // Target options
              style: 'expanded'
            },
            files: {                         // Dictionary of files
              'dist/base.css': 'scss/base/style.scss',       // 'destination': 'source'
              'dist/app.css': 'scss/app.scss'
            }
          }
      },

    concat: {
        options: {
          separator: ';',
          map:true
        },
        dist: {
          src: ['dist/3rdparty/*.js', 'dist/app.js', "dist/built.js"],
          dest: 'dist/main.js',
        },
    },
    ts: {
      default : {
        //src: ["**/*.ts", "!node_modules/**/*.ts"]
        files: [
            { src: ['typescript/**/*.ts','!typescript/app.ts'], dest: 'dist/built.js' },
            { src: ['typescript/app.ts'], dest: 'dist/app.js' }
        ],
        options: {
          fast: 'never'
        }
      }
    }

  });


    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks("grunt-ts");

    grunt.registerTask("default", ["ts", "concat:dist", "sass"]);

};
